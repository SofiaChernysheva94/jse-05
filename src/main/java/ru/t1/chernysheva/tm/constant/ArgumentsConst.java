package ru.t1.chernysheva.tm.constant;

public final class ArgumentsConst {

    public static final String HELP = "help";

    public static final String VERSION = "version";

    public static final String ABOUT = "about";

    private ArgumentsConst() {
    }

}
